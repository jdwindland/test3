package com.jdwindland;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//Class to create servlet. This servlet takes two numbers and adds, subtracts, multiplies and divides the numbers.
//The results are displayed on HTML page.
@WebServlet(name = "MemberServlet", urlPatterns ={"/MemberServlet"})
public class MemberServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws NumberFormatException {

        response.setContentType("text/html");

        try{
            PrintWriter writer = response.getWriter();
            writer.println("<html><head></head>");
            writer.println("<body>");
            int num1 = Integer.parseInt(request.getParameter("num1"));
            int num2 = Integer.parseInt(request.getParameter("num2"));
            int sum = num1+num2;
            int difference = num1-num2;
            int product = num1*num2;
            int quotient = num1/num2;
            writer.println("<h1>Let's do some Math!!!</h1>");
            writer.println("<p>The <b>sum</b> of " + num1 + " plus " + num2 + " is " + sum + ".</p>");
            writer.println("<p>The <b>difference</b> of " + num1 + " minus " + num2 + " is " + difference + ".</p>");
            writer.println("<p>The <b>product</b> of " + num1 + " times " + num2 + " is " + product + ".</p>");
            writer.println("<p>The <b>quotient</b> of " + num1 + " divided by " + num2 + " is " + quotient + ".</p>");
        }catch(NumberFormatException e){
            System.out.println("Please enter a valid number.");
        }catch(IOException e){
            System.out.println("There was a failure with the input and output operations.");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter writer = response.getWriter();
            response.setContentType("text/html");
            writer.println("Get Method");
        }catch(IOException e){
            System.out.println("There was a failure with the input and output operations.");
        }
    }
}
